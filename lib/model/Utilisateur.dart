import 'package:cloud_firestore/cloud_firestore.dart';

List<String> genres = ['homme', 'femme'];

class Utilisateur {
  String nom;
  String prenom;
  String genre;
  Timestamp dateNaissance;
  String password;
  String email;
  String nationalite;
  String tel;
  String photoProfile;

  Utilisateur({
    this.nom,
    this.prenom,
    this.genre,
    this.dateNaissance,
    this.password,
    this.email,
    this.nationalite,
    this.tel,
    this.photoProfile,
  });

  @override
  String toString() {
    return 'nom: $nom, prenom: $prenom, genre: $genre, age: $dateNaissance,, password: $password, email: $email, nationalite: $nationalite';
  }
}
