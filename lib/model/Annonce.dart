import 'dart:convert';

class Annonce {
  String id ;
  DateTime date;
  String description;
  String proRef;
  String titre;

  Annonce({this.id, this.date, this.description, this.proRef,this.titre});

  Map<String, dynamic> toMap() {
    return {
      'uid': id,
      'date': date,
      'description': description,
      'proRef': proRef,
      'titre' : titre,
    };
  }

  static Annonce fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    return Annonce(
      id: map['uid'],
      description: map['description'],
      date:map['date'].toDate(),
      proRef: map['proRef'],
      titre : map['titre'],
    );
  }

  String toJson() => json.encode(toMap());

  static Annonce fromJson(String source) => fromMap(json.decode(source));
}
