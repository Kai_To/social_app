import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:social_app/model/Annonce.dart';
import 'package:social_app/model/Utilisateur.dart';

class Professionnel extends Utilisateur {
  String proID;
  List<Annonce> annonces;
  String description;

  Professionnel({
    String nom,
    String prenom,
    String genre,
    Timestamp dateNaissance,
    String password,
    String email,
    String nationalite,
    String tel,
    String proID,
    String photoProfile,
    String description,
  }) : super(
            nom: nom,
            prenom: prenom,
            genre: genre,
            dateNaissance: dateNaissance,
            password: password,
            email: email,
            nationalite: nationalite,
            tel: tel,
            photoProfile: photoProfile) {
    this.proID = proID;
    this.description = description;
  }

  Map<String, dynamic> toMap() {
    return {
      'uid': proID,
      //'annonce': List<dynamic>.from(annonces.map((x) => x.toMap())),
      'photoProfile': photoProfile,
      'description': description,
      'dateNaissance': dateNaissance,
      'email': email,
      'nom': nom,
      'prenom': prenom,
      'password': password,
      'genre': genre,
      'nationalite': nationalite,
    };
  }

  static Professionnel fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return Professionnel(
      proID: map['uid'],
      email: map['email'],
      nom: map['nom'],
      prenom: map['prenom'],
      password: map['password'],
      genre: map['genre'],
      nationalite: map['nationalite'],
      dateNaissance: map['dateNaissance'],
      photoProfile: map['photoProfile'],
      description: map['description'],
      // List<Annonce>.from(map['annonces']?.map((x) => Annonce.fromMap(x))),
    );
  }

  String toJson() => json.encode(toMap());

  static Professionnel fromJson(String source) => fromMap(json.decode(source));
}
