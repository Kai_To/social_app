import 'dart:convert';

import 'package:social_app/model/Commentaire.dart';
import 'package:social_app/model/Professionnel.dart';

class Notifications {
  String uid;
  DateTime date;
  Commentaire commentaire;
  Professionnel professionnel;
  bool wasSeen ;

  Notifications({this.uid, this.date, this.commentaire, this.professionnel,this.wasSeen});

  Map<String, dynamic> toMap() {
    return {
      'uid': uid,
      'date': date,
      'commentaire': commentaire.toMap(),
      'professionnel': professionnel.toMap(),
      'wasSeen' : wasSeen,
    };
  }

  static Notifications fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    return Notifications(
      uid: map['uid'],
      date: map['date'].toDate(),
      commentaire: Commentaire.fromMap(map['commentaire']),
      professionnel: Professionnel.fromMap(map['professionnel']),
      wasSeen: map['wasSeen'],
    );
  }

  String toJson() => json.encode(toMap());
  static Notifications fromJson(String source) => fromMap(json.decode(source));
}
