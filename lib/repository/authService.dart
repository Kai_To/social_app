
import 'package:firebase_auth/firebase_auth.dart';
import 'package:social_app/main.dart';

import 'package:social_app/services/adminService.dart';
import 'package:social_app/services/professionelService.dart';
import 'package:social_app/services/talentService.dart';
import 'package:social_app/view/shared/constants.dart';

class AuthService {
  TalentService talentService;
  ProfessionelService professionelService;
  AdminService adminService;
  final FirebaseAuth _auth = FirebaseAuth.instance;
  AuthService(){
    
    this.talentService=TalentService();
    this.professionelService=ProfessionelService();
    this.adminService=AdminService();
  }

  
  Future signIn(String email, String password) async {
    int result=0;
    try {
      AuthResult authResult = await _auth.signInWithEmailAndPassword(
          email: email, password: password);
      FirebaseUser user = authResult.user;
      if(user!=null ){
        
         if(await talentService.exists(user.uid))MyApp.userType=UserType.talent;
         else if(await professionelService.exists(user.uid))MyApp.userType=UserType.professionnel;
         else if(await adminService.exists(user.uid)) MyApp.userType=UserType.admin;

         result=1;
      }
      
    } catch (e) {
      handleError(e);
    
    }
    return result;
  }
}
enum authProblems { UserNotFound, PasswordNotValid, NetworkError }


handleError(error){
var errorMessage='Error inconnue!';
switch(error.code){
  case 'ERROR_INVALID_EMAIL': errorMessage='Cet email est malformé!';break;
  case 'ERROR_WRONG_PASSWORD': errorMessage='Ce password est invalid!';break;
  case 'ERROR_USER_NOT_FOUND': errorMessage='Cet email n\'existe pas!';break;
  case 'ERROR_TOO_MANY_REQUESTS': errorMessage='Vous avez fait trop d\'attentatives!';break;
}
  return throw Exception(errorMessage);
}
