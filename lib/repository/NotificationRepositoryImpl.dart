import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:social_app/model/Notification.dart';
import 'package:social_app/repository/NotificationRepository.dart';
import 'package:uuid/uuid.dart';

final notifRef = Firestore.instance.collection('Notifications');

class NotificationRepositoryImpl implements NotificationRepository {
  @override
  Future<int> createNotification(Notifications notification) async {
    String notifId = Uuid().v4();
    int r = 0;
    try {
      await notifRef.document(notifId).setData({
        'uid': notifId,
        'date': notification.date,
        'commentaire': notification.commentaire.toMap(),
        'professionnel': notification.professionnel.toMap(),
        'wasSeen': false,
      });
      r = 1;
    } catch (e) {
      print(e);
    }
    return r;
  }

  @override
  Future<int> deleteNotification(String id) async {
    int r = 0;
    var doc = await notifRef.document(id).get();
    try {
      if (doc.exists) {
        doc.reference.delete();
        r = 1;
      }
    } catch (e) {
      print(e);
    }
    return r;
  }

  @override
  Future<int> updateNotification(Notifications notification) async {
    int r = 0;
    var doc = await notifRef.document(notification.uid).get();
    try {
      if (doc.exists) {
        doc.reference.updateData(notification.toMap());
        r = 1;
      }
    } catch (e) {
      print(e);
    }
    return r;
  }

  @override
  Future<List<Notifications>> getNotifs(String annonceId) async{
    List<Notifications> notifs = [];
    try {
      var result = await notifRef
          .where('commentaire.annonce.uid', isEqualTo: annonceId)
          .getDocuments();
      notifs = result.documents
          .map((doc) => Notifications.fromMap(doc.data))
          .toList();
    } catch (e) {
      print(e);
    }
    return notifs;
  }
}
