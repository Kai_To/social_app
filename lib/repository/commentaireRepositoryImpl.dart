import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:social_app/model/Commentaire.dart';
import 'package:social_app/repository/commentaireRepository.dart';
import 'package:uuid/uuid.dart';

final commentRef = Firestore.instance.collection('Commentaires');

class CommentaireRepositoryImpl implements CommentaireRepository {
  @override
  Future<int> createCommentaire(Commentaire commentaire) async {
    String commentId = new Uuid().v4();
    int r = 0;
    try {
      await commentRef.document(commentId).setData({
        'uid': commentId,
        'date': commentaire.date,
        'contenue': commentaire.contenue,
        'annonce': commentaire.annonce.toMap(),
        'talent': commentaire.talent.toMap(),
      });
      r = 1;
    } catch (e) {
      print(e);
    }
    return r;
  }

  @override
  Future<int> deleteCommentaire(String id) async {
    int r = 0;
    var doc = await commentRef.document(id).get();
    try {
      if (doc.exists) {
        doc.reference.delete();
        r = 1;
      }
    } catch (e) {
      print(e);
    }
    return r;
  }

  @override
  Future<int> updateCommentaire(Commentaire commentaire) async {
    int r = 0;
    var doc = await commentRef.document(commentaire.uid).get();
    try {
      if (doc.exists) {
        doc.reference.updateData(commentaire.toMap());
        r = 1;
      }
    } catch (e) {
      print(e);
    }
    return r;
  }

  @override
  Future<List<Commentaire>> getComments(String annonceId) async {
    List<Commentaire> comments = [];
    try {
      var result = await commentRef.where('annonce.uid',isEqualTo: annonceId).getDocuments();
      comments =
          result.documents.map((doc) => Commentaire.fromMap(doc.data)).toList();
    } catch (e) {
      print(e);
    }
    return comments;
  }
}
