import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:social_app/view/shared/constants.dart';
import 'package:video_player/video_player.dart';
import 'package:chewie/src/chewie_player.dart';
import 'package:social_app/model/Post.dart';
import 'package:social_app/model/Talent.dart';
import 'package:social_app/services/postService.dart';
import 'package:social_app/services/talentService.dart';
import 'package:social_app/view/profil/profil_page.dart';
import 'package:social_app/view/shared/custom_image.dart';
import 'package:social_app/view/profil/edit_post.dart';
import 'package:video_player/video_player.dart';

class PostWidget extends StatefulWidget {
  final Post post;
  final Talent poster;
  final bool canDelete;
  final UserType userType;

  PostWidget({
    @required this.poster,
    @required this.post,
    @required this.canDelete, 
    @required this.userType,
  });

  @override
  _PostWidgetState createState() => _PostWidgetState();
}

class _PostWidgetState extends State<PostWidget>/* with AutomaticKeepAliveClientMixin<PostWidget>*/ {
  PostService postService = PostService();
  TalentService talentService = TalentService();
  bool isLiked;
  String nom = '';
  String prenom = '';
  String photoProfil = '';
  String video = '';
  String posterId = '';
  String talentId = '';
  Talent talent = Talent();
  VideoPlayerController videoPlayerController;
  ChewieController chewieCtrl;
  Map likes = {};

  playVideo() {
    if (video != '') {
      videoPlayerController = VideoPlayerController.network(video);
      videoPlayerController.addListener(() {
      setState(() {});
    });
    videoPlayerController.initialize().then((_) => setState(() {}));
    videoPlayerController.pause();
      chewieCtrl = ChewieController(
        videoPlayerController: videoPlayerController,
        aspectRatio: videoPlayerController.value.aspectRatio,
        
      );
      
    }
  }

  getTalent() async {
    talent = await talentService.getCurrentUser();

    if (talent != null) talentId = talent.uid;
    posterId = widget.poster.uid;
    likes = widget.post.likes;
    setState(() {
      isLiked = (likes[talentId] == true || likes[posterId] == true);
    });
  }
  

  @override
  void initState() {
    super.initState();
    getTalent();

    photoProfil = widget.poster.photoProfile;
    nom = widget.poster.nom;
    prenom = widget.poster.prenom;
    video = widget.post.videoUrl;
    playVideo();
    
  }

  @override
  void dispose() {
     chewieCtrl.dispose();
     videoPlayerController.dispose();
    super.dispose();
  }

  handleLikePost() async {
    bool _isLiked = (likes[talentId] == true);
    print(_isLiked);
    if (_isLiked) {
      try {
        await postService.likeProfilPosts(talentId, widget.post.postId, false);
        setState(() {
          isLiked = false;
          likes[talentId] = false;
        });
      } catch (e) {
        print(e);
      }
    } else if (!_isLiked) {
      try {
        await postService.likeProfilPosts(talentId, widget.post.postId, true);
        setState(() {
          isLiked = true;
          likes[talentId] = true;
        });
      } catch (e) {
        print(e);
      }
    }
  }

  goToProfile() {
    if(widget.userType == UserType.talent){
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ProfilPage(
                  poster: widget.poster,
                  visitor: UserType.talent,
                  isProfil: false,
                )));
    }
    else if(widget.userType == UserType.professionnel){
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ProfilPage(
                  poster: widget.poster,
                  visitor: UserType.professionnel,
                  isProfil: false,
                )));      
    }
  }

  deletePost() async {
    int r = await postService.deletePost(widget.post.postId);
    print(r);
    Navigator.of(context).pop();
  }

  deletePostAlert(context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: new Text("Etes-vous sure?"),
          actions: <Widget>[
            new FlatButton(
              child: new Text("supprimer"),
              onPressed: () => deletePost(),
            ),
            new FlatButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  modifierPost(context) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => EditPost(
                  post: widget.post,
                  talent: widget.poster,
                )));
  }

  buildPostHeader() {
    return ListTile(
        leading: GestureDetector(
          onTap: () => goToProfile(),
          child: CircleAvatar(
            backgroundColor: Colors.red,
            backgroundImage: photoProfil != ''
                ? CachedNetworkImageProvider(photoProfil)
                : AssetImage('assets/images/user_icon.png'),
          ),
        ),
        title: GestureDetector(
          child: Text(
            prenom + ' ' + nom,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 18,
            ),
          ),
        ),
        subtitle: Text(widget.post.description),
        trailing: widget.canDelete == true
            ? PopupMenuButton<int>(
                onSelected: (value) {
                  if (value == 1)
                    deletePostAlert(context);
                  else if (value == 2) modifierPost(context);
                },
                itemBuilder: (BuildContext context) => [
                      PopupMenuItem(
                        value: 1,
                        child: Text('Supprimer'),
                      ),
                      PopupMenuItem(
                        value: 2,
                        child: Text('modifier'),
                      ),
                    ])
            : null
        );
  }

  buildPostImage() {
    if (widget.post.photoUrl != '')
      return GestureDetector(
          child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          cachedNetworkImage(widget.post.photoUrl),
        ],
      ));

    if (video != '')
      return GestureDetector(
        child: Stack(
          alignment: Alignment.center,
          children: <Widget>[
            Chewie(
              controller: chewieCtrl,
            ),
          ],
        ),
      );
  }

  buildPostFooter() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        GestureDetector(
          onTap: () => handleLikePost(),
          child:widget.userType != UserType.professionnel? Icon(
            isLiked ? Icons.favorite : Icons.favorite_border,
            size: 25,
          ):Text(''),
        ),
        SizedBox(
          width: 4,
        ),
        Container(
          child: Text(
            widget.post.getLikesCount().toString() + ' likes',
            style: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.bold,
            ),
          ),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    //super.build(context);
    isLiked = (likes[talentId] == true);

    return Container(
      //height: MediaQuery.of(context).size.height,
      margin: EdgeInsets.symmetric(horizontal: 0, vertical: 1),
      decoration: BoxDecoration(
        color: Colors.white, //Color(0xFFF0F0F0),
        border: Border.all(color: Colors.blueGrey, width: 1),
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 1, vertical: 0),
        child: InkWell(
          child: Column(
            children: <Widget>[
              buildPostHeader(),
              buildPostImage(),
              SizedBox(
                height: 10,
              ),
              buildPostFooter(),
              SizedBox(
                height: 10,
              )
            ],
          ),
        ),
      ),
    );
  }

/*  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;*/
}
