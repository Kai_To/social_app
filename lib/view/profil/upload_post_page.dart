import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:image_picker/image_picker.dart';
import 'package:social_app/model/Talent.dart';
import 'package:social_app/repository/talentAuth.dart';
import 'package:social_app/services/postService.dart';
import 'package:social_app/services/talentService.dart';
import 'package:social_app/view/shared/constants.dart';
import 'package:social_app/view/shared/progress.dart';
import 'package:video_player/video_player.dart';

class UploadPost extends StatefulWidget {
  final witchPost post;
  final String nom;
  UploadPost({ this.nom,this.post});

  @override
  _UploadPostState createState() => _UploadPostState();
}

class _UploadPostState extends State<UploadPost> {
  
  TalentService talentService = TalentService();
  PostService postService = PostService();
  TalentAuth talentAuth = TalentAuth();
  Talent talent = Talent();
  String photoUrl = '';
  File image;
  File video;

  VideoPlayerController videoPlayerController;

  TextEditingController captionController = TextEditingController();
  bool isUploading = false;

  addImage() async {
    Navigator.pop(context);
    File image = await ImagePicker.pickImage(
        source: ImageSource.gallery, imageQuality: 50);
    setState(() {
      this.image = image;
    });
    print(image);
  }

  addVideo() async {
    Navigator.pop(context);
    File video = await ImagePicker.pickVideo(source: ImageSource.gallery);
    setState(() {
      this.video = video;
    });
    print(video);
    videoPlayerController = VideoPlayerController.file(video)
      ..initialize().then((_) {
        setState(() {});
        videoPlayerController.play();
      });
  }

  selectPost(context) {
    return showDialog(
        context: context,
        builder: (context) {
          return SimpleDialog(
            title: Text(
              widget.post == witchPost.profilePic ? 'ajouter photo de profile' : 'créer un poste',
            ),
            children: <Widget>[
              SimpleDialogOption(
                child: Text('image'),
                onPressed: () => addImage(),
              ),
              if (widget.post != witchPost.profilePic)
                SimpleDialogOption(
                  child: Text('video'),
                  onPressed: () => addVideo(),
                )
            ],
          );
        });
  }

  Container buildSplashScrean() {
    return Container(
      color: Color(0xFF009688),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SvgPicture.asset(
            'assets/images/upload.svg',
            height: 260,
          ),
          Padding(
            padding: EdgeInsets.only(top: 20),
            child: RaisedButton(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8)),
              child: Text('Upload post',
                  style: TextStyle(color: Colors.white,fontFamily: 'JosefinSans',fontWeight: FontWeight.bold,
                  fontSize: 20)),
              color: Colors.deepOrange,
              onPressed: () => selectPost(context),
            ),
          )
        ],
      ),
    );
  }

  clearPost() {
    setState(() {
      image = null;
    });
  }

  handleSubmit() async {
    setState(() {
      isUploading = true;
    });
    if (image != null) {
      await postService.handleSubmitImage(image, captionController, talent.uid);
    }
    if (video != null) {
      await postService.handleSubmitVideo(video, captionController, talent.uid);
    }
    captionController.clear();

    setState(() {
      image = null;
      video = null;
      isUploading = false;
    });
  }



  handleSubmitProfileImage() async {
    setState(() {
      isUploading = true;
    });
    if (image != null) {
      await talentService.handleSubmitProfileImage(image, talent.uid);
    }
    setState(() {
      image = null;
      isUploading = false;
    });
  }

  handleSunmitFirstPost() async { 
    String mediaUrl;
    setState(() {
      isUploading = true;
    });
    if (image != null) {
      mediaUrl = await talentAuth.handleSubmitFirstPost(image, widget.nom);
    }
    if (video != null) {
      mediaUrl = await talentAuth.handleSubmitFirstPost(image, widget.nom);
    }

    setState(() {
      image = null;
      isUploading = false;
    });
    Navigator.pop(context, mediaUrl);
  }

  Scaffold buildUploadForm() {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white70,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Colors.black,
          ),
          onPressed: clearPost,
        ),
        title: Text(widget.post == witchPost.profilePic ? 'Photo de profile' : 'Confirmation',
            style: TextStyle(color: Colors.black,
            fontFamily: 'JosefinSans',fontSize:widget.post == witchPost.profilePic ?20 : 29,
            fontWeight: FontWeight.bold)),
        actions: [
          if (widget.post != witchPost.firstPost)
            FlatButton(
              child: Text( widget.post == witchPost.profilePic ? 'Terminer' : 'Publier',
                  style: TextStyle(
                      color: Colors.blueAccent,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'JosefinSans',
                      fontSize: 20)),
              onPressed: widget.post == witchPost.profilePic // isUploading
                  ? () => handleSubmitProfileImage()
                  : () => handleSubmit(),
            )
        ],
      ),
      body: ListView(
        children: <Widget>[
          isUploading ? linearProgress() : Text(''),
          if (image != null)
            Container(
              height: 300,
              width: MediaQuery.of(context).size.width * 0.8,
              child: Center(
                child: AspectRatio(
                  aspectRatio: 16 / 16,
                  child: Container(
                    decoration: BoxDecoration(
                      border: Border.all(
                        width: 1,
                      ),
                      image: DecorationImage(
                        fit: BoxFit.cover,
                        image: FileImage(image),
                      ),
                    ),
                  ),
                ),
              ),
            ),

          if (video != null)
            videoPlayerController.value.initialized
                ? Container(
                    //height: 300,
                    // width: MediaQuery.of(context).size.width * 0.8,
                    child: Center(
                      child: AspectRatio(
                        aspectRatio: videoPlayerController.value.aspectRatio,
                        child: VideoPlayer(videoPlayerController),
                      ),
                    ),
                  )
                : Container(),
          SizedBox(
            height: 8,
          ),
          if (widget.post == witchPost.newPost)
            ListTile(
              leading: CircleAvatar(
              backgroundImage: photoUrl != ''
                  ? CachedNetworkImageProvider(photoUrl)
                  : AssetImage('assets/images/user_icon.png'),
                backgroundColor: Colors.blueGrey,
              ),
              title: Container(
                width: 250,
                child: TextField(
                  controller: captionController,
                  decoration: InputDecoration(
                    hintText: "écris une discription...",
                    border: InputBorder.none,
                  ),
                ),
              ),
            ),
          if (widget.post == witchPost.firstPost)
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 80, vertical: 40),
              child: RaisedButton(
                onPressed: () {
                  handleSunmitFirstPost();
                },
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30)),
                child: Text('Terminer',
                    style: TextStyle(fontSize: 25.0,fontFamily: 'JosefinSans',
                    color: Colors.white,fontWeight: FontWeight.bold)),
              ),
            )
        ],
      ),
    );
  }

  checkTalent() async {
    talent = await talentService.getCurrentUser();
    photoUrl = talent.photoProfile;
    print(photoUrl);
  }

  @override
  void initState() {
    if (widget.post != witchPost.firstPost) checkTalent();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return image == null && video == null ? buildSplashScrean() : buildUploadForm();
  }
}
