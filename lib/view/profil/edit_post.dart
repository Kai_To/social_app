import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:chewie/chewie.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:social_app/model/Post.dart';
import 'package:social_app/model/Talent.dart';
import 'package:social_app/view/shared/custom_image.dart';
import 'package:social_app/view/shared/progress.dart';
import 'package:video_player/video_player.dart';
import 'package:social_app/services/postService.dart';

class EditPost extends StatefulWidget {
  final Post post;
  final Talent talent;

  EditPost({this.post, this.talent});

  @override
  _EditPostState createState() => _EditPostState();
}

class _EditPostState extends State<EditPost> {
  PostService postService = PostService();
  bool isUploading = false;
  VideoPlayerController videoPlayerController;
  TextEditingController captionController = TextEditingController();
  VideoPlayerController videoPlayerController2;
  ChewieController chewieCtrl;
  
  File image;
  File video;

  @override
  void initState() {
    captionController = TextEditingController(text :widget.post.description);
    playVideo();
    super.initState();
  }

  addImage() async {
    File image = await ImagePicker.pickImage(
        source: ImageSource.gallery, imageQuality: 50);
    setState(() {
      this.image = image;
    });
  }

  addVideo() async {
    File video = await ImagePicker.pickVideo(source: ImageSource.gallery);
    setState(() {
      this.video = video;
    });
    videoPlayerController2 = VideoPlayerController.file(video)
      ..initialize().then((_) {
        setState(() {});
        videoPlayerController2.play();
      });
  }

  playVideo() {
    if (widget.post.videoUrl != '') {
      print('${widget.post.videoUrl} hello');
      videoPlayerController =
          VideoPlayerController.network(widget.post.videoUrl);
      chewieCtrl = ChewieController(
        videoPlayerController: videoPlayerController,
        autoInitialize: true,
        aspectRatio: videoPlayerController.value.aspectRatio,
      );
    }
  }

  @override
  void dispose() {
    if (chewieCtrl != null) chewieCtrl.dispose();
    if (videoPlayerController != null) videoPlayerController.dispose();
    super.dispose();
  }

  handleSunmitupdatePost() async {
    setState(() {
      isUploading = true;
    });
    if (image != null) {
      await postService.updatePostImage(
          image, captionController.text, widget.post);
    }
    if (video != null) {
      await postService.updatePostVideo(
          video, captionController.text, widget.post);
    }
    if (image == null && video == null) {
      await postService.updatePost(Post(
        description: captionController.text,
        photoUrl: widget.post.photoUrl,
        videoUrl: widget.post.videoUrl,
        postId: widget.post.postId,
        talentId: widget.post.talentId,
        date: widget.post.date,
        likes: widget.post.likes,
      ));
    }
    captionController.clear();

    setState(() {
      isUploading = false;
      Navigator.pop(context);
    });
  }

  selectPost(context) {
    return showDialog(
        context: context,
        builder: (context) {
          return SimpleDialog(
            children: <Widget>[
              SimpleDialogOption(
                child: Text('image'),
                onPressed: () => addImage(),
              ),
              SimpleDialogOption(
                child: Text('video'),
                onPressed: () => addVideo(),
              )
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white70,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Colors.black,
          ),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text('Modifer Post',
            style: TextStyle(
              color: Colors.black,
              fontFamily: 'JosefinSans',
              fontSize: 29.0,
              fontWeight: FontWeight.bold,
            )),
        actions: [
          PopupMenuButton<int>(
            itemBuilder: (context) => [
              PopupMenuItem(
                value: 1,
                child: Text("Image"),
              ),
              PopupMenuItem(
                value: 2,
                child: Text("Video"),
              ),
            ],
            onSelected: (value) {
                  if (value == 1) addImage();
                  else if (value == 2) addVideo();              
            },
            child: Center(child: Text('change post',
            style: TextStyle(
              fontWeight: FontWeight.bold,
              color: Colors.blueAccent,
              fontSize: 17
            ),)),
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            isUploading ? linearProgress() : Text(''),
            if (widget.post.photoUrl != '' && image == null && video == null)
              Container(
                height: 300,
                width: MediaQuery.of(context).size.width * 0.8,
                child: Center(
                  child: AspectRatio(
                    aspectRatio: 16 / 16,
                    child: GestureDetector(
                      child: Stack(
                        alignment: Alignment.center,
                        children: <Widget>[
                          cachedNetworkImage(widget.post.photoUrl),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            if (image != null)
              Container(
                height: 300,
                width: MediaQuery.of(context).size.width * 0.8,
                child: Center(
                  child: AspectRatio(
                    aspectRatio: 16 / 16,
                    child: Container(
                      decoration: BoxDecoration(
                        border: Border.all(
                          width: 1,
                        ),
                        image: DecorationImage(
                          fit: BoxFit.cover,
                          image: FileImage(image),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            if (widget.post.videoUrl != '' && image == null && video == null)
              GestureDetector(
                child: Stack(
                  alignment: Alignment.center,
                  children: <Widget>[
                    Chewie(
                      controller: chewieCtrl,
                    ),
                  ],
                ),
              ),
            if (video != null)
              videoPlayerController2.value.initialized
                  ? Container(
                      child: Center(
                        child: AspectRatio(
                          aspectRatio: videoPlayerController2.value.aspectRatio,
                          child: VideoPlayer(videoPlayerController2),
                        ),
                      ),
                    )
                  : Container(),
            ListTile(
              leading: CircleAvatar(
                backgroundImage: widget.talent.photoProfile != ''
                    ? CachedNetworkImageProvider(widget.talent.photoProfile)
                    : AssetImage('assets/images/user_icon.png'),
                backgroundColor: Colors.blueGrey,
              ),
              title: Container(
                width: 250,
                child: TextFormField(
                  //initialValue: widget.post.description == null ? '' : widget.post.description,
                  controller: captionController,
                  decoration: InputDecoration(
                    hintText: "write a discription...",
                    border: InputBorder.none,
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 0, vertical: 10),
              child: RaisedButton(
                color: Colors.blueAccent,
                onPressed: () {
                  handleSunmitupdatePost();
                },
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30)),
                child: Text('Enregistrer',
                    style: TextStyle(
                        fontSize: 20.0,
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                        fontFamily: 'JosefinSans')),
              ),
            )
          ],
        ),
      ),
    );
  }
}
