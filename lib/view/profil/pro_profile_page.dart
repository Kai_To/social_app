import 'package:flutter/material.dart';
import 'dart:io';
import 'package:image_picker/image_picker.dart';
import 'package:social_app/model/Annonce.dart';
import 'package:social_app/model/Professionnel.dart';
import 'package:social_app/model/Talent.dart';
import 'package:social_app/services/annonceService.dart';
import 'package:social_app/services/professionelService.dart';
import 'package:social_app/services/talentService.dart';
import 'package:social_app/view/accueil/display_annonce.dart';
import 'package:social_app/view/shared/constants.dart';
import 'package:social_app/view/shared/progress.dart';
import 'package:social_app/view/shared/reusable_header.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:cached_network_image/cached_network_image.dart';

class ProProfilPage extends StatefulWidget {
  static const String id = 'proProfilPage';
  @override
  _ProProfilPageState createState() => _ProProfilPageState();
}

class _ProProfilPageState extends State<ProProfilPage> {
  Professionnel pro = Professionnel();
  bool isWaiting = false;
  bool isWaitingForAnnonces = false;
  ProfessionelService professionelService = ProfessionelService();
  Talent talent = Talent();
  TalentService talentService = TalentService();
  AnnonceService annonceService = AnnonceService();
  String nom = 'NaN';
  String prenom = 'NaN';
  String email = '';
  String nationalite = '';
  String description = '';
  String photoUrl = ' ';
  String id = '';
  String proDesc = '';
  List<Widget> annoncesWidgets = [];
  File image;
  bool isUploading = false;
  String profilImage = '';
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      new GlobalKey<RefreshIndicatorState>();

  addImage(context) async {
    File image = await ImagePicker.pickImage(
        source: ImageSource.gallery, imageQuality: 50);
    setState(() {
      this.image = image;
    });
  }

  handleSunmitImage() async {
    setState(() {
      isUploading = true;
    });
    if (image != null) {
      await professionelService.handleSubmitProfileImage(image, id);
    }
    setState(() {
      image = null;
      isUploading = false;
    });
  }

  @override
  initState() {
    super.initState();
    isWaiting = true;
    isWaitingForAnnonces = true;
    getProfileContent();
  }

  @override
  dispose() {
    isWaitingForAnnonces = false;
    isWaiting = false;
    super.dispose();
  }

  getProfileContent() async {
    pro = await professionelService.getCurrentPro();
    nom = pro.nom;
    proDesc = pro.description;
    prenom = pro.prenom;
    email = pro.email;
    nationalite = pro.nationalite;
    id = pro.proID;
    profilImage = pro.photoProfile;
    print(id);
    setState(() {
      isWaiting = false;
    });
    getProfilAnnonces();
  }

  getProfilAnnonces() async {
    List<Annonce> tempList = await annonceService.searchByUser(id);
    if (tempList.isNotEmpty)
      for (Annonce an in tempList) {
        print('description:' + an.description);
        annoncesWidgets.add(DisplayAnnonce(
          isProf: true,
          annonce: an,
          prof: pro,
          userType: UserType.professionnel,
        ));
      }
    setState(() {
      isWaitingForAnnonces = false;
    });
  }

  ScrollController controller;

  Widget buildUploadForm(context) {
    return ModalProgressHUD(
      child: Scaffold(
        body: ListView(
          children: <Widget>[
            if (image != null)
              Container(
                margin: EdgeInsets.only(top: 20),
                height: 300,
                width: MediaQuery.of(context).size.width * 0.8,
                child: Center(
                  child: AspectRatio(
                    aspectRatio: 16 / 16,
                    child: Container(
                      decoration: BoxDecoration(
                        border: Border.all(
                          width: 1,
                        ),
                        image: DecorationImage(
                          fit: BoxFit.contain,
                          image: FileImage(image),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            SizedBox(
              height: 8,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 80, vertical: 40),
              child: RaisedButton(
                onPressed: () async {
                  setState(() {
                    isWaiting = true;
                  });
                  await handleSunmitImage();
                  setState(() {
                    isWaiting = false;
                  });
                },
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30)),
                child: Text('termier',
                    style: TextStyle(fontSize: 20.0, color: Colors.white)),
              ),
            )
          ],
        ),
      ),
      inAsyncCall: isWaiting,
    );
  }

  @override
  Widget build(BuildContext context) {
    return image != null
        ? buildUploadForm(context)
        : Scaffold(
            appBar: header(context, "profile", 'Artiness'),
            backgroundColor: Colors.grey[200],
            body: ModalProgressHUD(
              child: RefreshIndicator(
                onRefresh: () async{
                 
                    isWaiting = true;
                    isWaitingForAnnonces = true;
                  
                  refresh();
                },
                child: buildProfileContent(context),
              ),
              inAsyncCall: isWaiting,
            ));
  }

  Future<void> refresh() async {
    setState(() {
      isWaiting=true;
    });
    this.annoncesWidgets=[];
    await getProfileContent();
    
  
    
  }

  ListView buildProfileContent(BuildContext context) {
    return ListView(
      children: <Widget>[
        Stack(children: <Widget>[
          Column(children: <Widget>[
            Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/images/Dance.jpg"),
                    fit: BoxFit.cover),
              ),
              height: MediaQuery.of(context).size.height / 5,
            ),
            SizedBox(height: 10.0),
            Container(
              margin: EdgeInsets.only(left: 20),
              child: Text(
                nom + ' ' + prenom,
                style: TextStyle(
                    fontSize: 25.0,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Montserrat'),
              ),
            ),
          ]),
          Positioned(
            width: 340.0,
            top: MediaQuery.of(context).size.height / 7,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                    margin: EdgeInsets.only(left: 20),
                    width: 75.0,
                    height: 75.0,
                    decoration: BoxDecoration(
                        color: Colors.red,
                        image: DecorationImage(
                            image: profilImage == ''
                                ? AssetImage('assets/images/user_icon.png')
                                : CachedNetworkImageProvider(profilImage),
                            fit: BoxFit.cover),
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        boxShadow: [
                          BoxShadow(blurRadius: 7.0, color: Colors.black)
                        ])),
              ],
            ),
          ),
        ]),
        Column(
          children: <Widget>[
            SizedBox(height: 3.0),
            Text(
              nationalite,
              style: TextStyle(
                  fontSize: 15.0,
                  fontStyle: FontStyle.italic,
                  fontFamily: 'Montserrat'),
            ),
            SizedBox(height: 20.0),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 20),
              //height: 200,
              decoration: BoxDecoration(
                color: Colors.white70,
                borderRadius: BorderRadius.all(Radius.circular(10)),
              ),
              padding: EdgeInsets.all(20),
              child: Text(
                proDesc,
                style: TextStyle(
                  color: Colors.black87,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
            SizedBox(height: 10.0),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 20),
              height: 40,
              width: MediaQuery.of(context).size.width - 40,
              decoration: BoxDecoration(
                color: Colors.blueAccent,
                borderRadius: BorderRadius.all(Radius.circular(10)),
              ),
              child: RaisedButton(
                  child: Text(
                    'Ajouter photo de profie',
                    style: TextStyle(
                        fontFamily: 'JosefinSans',
                        color: Colors.white,
                        fontWeight: FontWeight.w900,
                        fontSize: 22),
                  ),
                  color: Colors.blue[200],
                  onPressed: () {
                    addImage(context);
                  }),
            ),
            SizedBox(height: 10.0),
            isWaitingForAnnonces
                ? circularProgress()
                : Container(
                    child: Column(children: annoncesWidgets),
                  )
          ],
        )
      ],
    );
  }
}
