import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:social_app/view/profil/edit_profile_page.dart';

import 'constants.dart';

AppBar header(context, String page,String title) {
  return AppBar(
    elevation: 0.0,
    title: Text(
      title,
      style: kAdminHeaderTextStyle,
    ),

    actions: <Widget>[
      if (page == "profil" || page == 'actualite' || page=='admin')
        new IconButton(
            icon: page == "profil" ? Icon(Icons.edit) : Icon(Icons.exit_to_app),
            onPressed: () {
              if (page == "profil")
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => EditProfilPage()));
              else {
                FirebaseAuth.instance.signOut();
                Navigator.of(context).pop(null);
              }
            }),
    ],
    leading: new Container(),
    centerTitle: true,
    backgroundColor: Colors.purple[800]// Color(0xFF1D1E33),
  );
}
