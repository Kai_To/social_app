import 'package:flutter/material.dart';

class CreateField extends StatelessWidget {
  final Function change;
  final String fieldName;

  CreateField({@required this.change, @required this.fieldName});
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      keyboardType: TextInputType.text,
      validator: (value)=> value.length <3 ? "$fieldName doit contenir au moins 3 caractères!!" : null,
      style: TextStyle(
          fontSize: 17, color: Colors.black, fontWeight: FontWeight.bold),
      decoration: InputDecoration(
        border:
            UnderlineInputBorder(), //OutlineInputBorder(borderRadius: BorderRadius.circular(30)),
        labelText: fieldName,
      ),
      onChanged: change,
    );
  }
}

class CreateLogInField extends StatelessWidget {
  final Function onChange;
  final String type;

  CreateLogInField({@required this.type, @required this.onChange});

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      cursorColor: Colors.purple,
      maxLines: 1,
      keyboardType:
          type == "Email" ? TextInputType.emailAddress : TextInputType.text,
      autofocus: false,
      onChanged: onChange,
      //validator: (val) => val.isEmpty || val.length<6 ? 'can\'t be empty' : null,
      obscureText: type == "Email" ? false : true,
      style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
      decoration: InputDecoration(
          focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.purple)),
          focusColor: Colors.black,
          fillColor: Colors.black,
          hoverColor: Colors.black,
          hintText: type,
          hintStyle: TextStyle(color: Colors.black38),
          icon: Icon(
            type == "Email" ? Icons.mail : Icons.lock,
            color: Colors.grey,
            size: 20,
          ),
          border: UnderlineInputBorder()),
    );
  }
}



class PhoneField extends StatelessWidget {
  final Function onChange;

  const PhoneField({this.onChange});
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      keyboardType: TextInputType.phone,
      validator: validatePhone,
      style: TextStyle(
          fontSize: 17, color: Colors.black, fontWeight: FontWeight.bold),
      decoration: InputDecoration(
        border:
            UnderlineInputBorder(), //OutlineInputBorder(borderRadius: BorderRadius.
        labelText: "Telephone",
      ),
      onChanged: onChange,
    );
  }
}


class EmailField extends StatelessWidget {
  final Function onChange;

  const EmailField({this.onChange});
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      validator: validateEmail,
      style: TextStyle(
          fontSize: 17, color: Colors.black, fontWeight: FontWeight.bold),
      decoration: InputDecoration(
        border:
            UnderlineInputBorder(), //OutlineInputBorder(borderRadius: BorderRadius.
        labelText: "Email",
      ),
      onChanged: onChange,
    );
  }
}


class PasswordField extends StatelessWidget {
  final Function onChange;

  const PasswordField({this.onChange});
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      keyboardType: TextInputType.text,
      obscureText: true,
      validator: validatePassword,
      style: TextStyle(
          fontSize: 17, color: Colors.black, fontWeight: FontWeight.bold),
      decoration: InputDecoration(
        border:
            UnderlineInputBorder(), //OutlineInputBorder(borderRadius: BorderRadius.
        labelText: "Mot de passe",
      ),
      onChanged: onChange,
    );
  }
}


String validateEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value))
      return 'Enter un email valide';
    else
      return null;
  }

String validatePassword(String value) {
    if (value.length < 6)
      return 'le mot de passe doit être formé d\'au moins 6 caractères';
    else
      return null;
  }

String validatePhone(String value) {
  if (value.length != 10)
      return 'Le numero de telephone doit être de 10 nombres';
    else
      return null;
  }

