import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/material.dart';
import 'package:flutter_country_picker/country.dart';
import 'package:flutter_country_picker/flutter_country_picker.dart';
import 'package:social_app/model/Categorie.dart';
import 'package:social_app/repository/talentAuth.dart';
import 'package:social_app/services/categorieService.dart';
import 'package:social_app/view/accueil/home.dart';
import 'package:social_app/view/profil/upload_post_page.dart';
import 'package:social_app/view/shared/constants.dart';
import 'package:social_app/view/shared/loading.dart';
import 'package:social_app/view/shared/reusable_text_field.dart';
import 'package:intl/intl.dart';

class InfosPage extends StatefulWidget {
  final String email;
  final String password;

  InfosPage({@required this.email, @required this.password});

  @override
  _InfosPageState createState() => _InfosPageState();
}

class _InfosPageState extends State<InfosPage> {
  final TalentAuth _auth = TalentAuth();
  final _formKey = GlobalKey<FormState>();
  CategorieService categorieService = CategorieService();
  String error = '';
  String nom = '';
  String prenom = '';
  String genre = '';
  String nationalite = '';
  String tel = '';
  String photoUrl = '';
  String videoUrl = '';
  String description = '';
  bool loading = false;
  String selected;
  List<String> catsName = [];
  String selectedCat;
  Country countSelected;
  DateTime dateNaissance;
  final format = DateFormat("yyyy-MM-dd");
  TextEditingController commentController = TextEditingController();

  @override
  void initState() {
    print(photoUrl);
    allCategories();
    super.initState();
  }

  allCategories() async {
    List<Categorie> categories = await categorieService.allCategories();
    for (Categorie cat in categories) {
      setState(() {
        catsName.add(cat.cat);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Padding(
        padding: const EdgeInsets.fromLTRB(8, 40, 8, 0),
        child: Form(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Text(
                'Talent',
                style: TextStyle(
                  fontSize: 40,
                  color: Colors.blueGrey,
                  fontFamily: 'JosefinSans',
                  fontWeight: FontWeight.bold,
                ),
              ),
              Expanded(
                child: Row(
                  children: <Widget>[
                    Expanded(
                        child: CreateField(
                      fieldName: "Nom",
                      change: (val) {
                        nom = val;
                      },
                    )),
                    SizedBox(
                      width: 15,
                    ),
                    Expanded(
                        child: CreateField(
                      fieldName: "Prenom",
                      change: (val) {
                        prenom = val;
                      },
                    )),
                  ],
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Expanded(
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: DropdownButtonFormField<String>(
                          isDense: true,
                          validator: (val) =>
                              val == null ? 'choisissez votre genre' : null,
                          style: TextStyle(
                              fontSize: 17,
                              color: Colors.black,
                              fontWeight: FontWeight.bold),
                          decoration: InputDecoration(
                            hintText: 'Genre',
                            hintStyle: TextStyle(
                                fontSize: 17,
                                color: Colors.grey[600],
                                fontWeight: FontWeight.bold),
                          ),
                          value: selected,
                          items: ["homme", "femme"]
                              .map((label) => DropdownMenuItem(
                                    child: Text(label),
                                    value: label,
                                  ))
                              .toList(),
                          onChanged: (value) {
                            setState(() => selected = value);
                          }),
                    ),
                    SizedBox(
                      width: 15,
                    ),
                    Expanded(
                      child: DateTimeField(
                        validator: (val) => val == null
                            ? 'choisissez votre date de naissance'
                            : null,
                        decoration: InputDecoration(
                          hintText: 'Date de Nissance',
                          hintStyle: TextStyle(
                              fontSize: 17,
                              color: Colors.grey[600],
                              fontWeight: FontWeight.bold),
                        ),
                        format: format,
                        onShowPicker: (context, currentValue) {
                          return showDatePicker(
                              context: context,
                              firstDate: DateTime(1900),
                              initialDate: currentValue ?? DateTime.now(),
                              lastDate: DateTime(2100));
                        },
                        onChanged: (val) {
                          dateNaissance = val;
                          print(dateNaissance);
                        },
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Expanded(
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: DropdownButtonFormField<String>(
                          isDense: true,
                          validator: (val) =>
                              val == null ? 'choisissez une categorie' : null,
                          style: TextStyle(
                              fontSize: 17,
                              color: Colors.black,
                              fontWeight: FontWeight.bold),
                          decoration: InputDecoration(
                              hintText: 'Categorie',
                              hintStyle: TextStyle(
                                  fontSize: 17,
                                  color: Colors.grey[600],
                                  fontWeight: FontWeight.bold)),
                          value: selectedCat,
                          items: catsName
                              .map((label) => DropdownMenuItem(
                                    child: Text(label),
                                    value: label,
                                  ))
                              .toList(),
                          onChanged: (value) {
                            setState(() {
                              selectedCat = value;
                              print(selectedCat);
                            });
                          }),
                    ),
                    SizedBox(
                      width: 15,
                    ),
                    Expanded(child: PhoneField(
                      onChange: (val) {
                        tel = val;
                      },
                    )),
                  ],
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Expanded(
                  child: CreateField(
                fieldName: "Description",
                change: (val) {
                  description = val;
                },
              )),
              SizedBox(
                height: 15,
              ),
              Expanded(
                child: CountryPicker(
                  currencyISOTextStyle: TextStyle(
                      fontSize: 17,
                      color: Colors.black,
                      fontWeight: FontWeight.bold),
                  currencyTextStyle: TextStyle(
                      fontSize: 17,
                      color: Colors.grey[600],
                      fontWeight: FontWeight.bold),
                  nameTextStyle: TextStyle(
                      fontSize: 17,
                      color: Colors.black,
                      fontWeight: FontWeight.bold),
                  dense: false,
                  showFlag: true, //displays flag, true
                  showDialingCode: false, //displays dialing co
                  showName: true, //displays country nam
                  showCurrency: false, //eg. 'Brit
                  showCurrencyISO: true, //eg. 'GB
                  onChanged: (Country country) {
                    setState(() {
                      countSelected = country;
                    });
                  },
                  selectedCountry: countSelected,
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Expanded(
                child: ListTile(
                  contentPadding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                  title: photoUrl == ''
                      ? Text(
                          'Ajoute une photo ou vidéo pour que votre profil soit validé',
                          style: TextStyle(
                              fontSize: 15,
                              color: Colors.grey[600],
                              fontWeight: FontWeight.bold),
                        )
                      : Text(
                          photoUrl,
                          style: TextStyle(
                            color: Colors.red,
                          ),
                        ),
                  trailing: GestureDetector(
                      onTap: () async {
                        if (_formKey.currentState.validate()) {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => UploadPost(
                                        post: witchPost.firstPost,
                                        nom: nom,
                                      ))).then((val) {
                            setState(() {
                              photoUrl = val;
                            });
                          });
                        }
                      },
                      child: Icon(
                        Icons.add_a_photo,
                        color: Colors.lightBlueAccent,
                        size: 40,
                      )),
                ),
              ),
              Divider(
                height: 4,
                color: Colors.black87,
              ),
              SizedBox(
                height: 30,
              ),
              RaisedButton(
                highlightColor: Colors.blue,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30)),
                color: Colors.lightBlueAccent,
                child: Text('Register',
                    style: TextStyle(
                        fontSize: 25.0,
                        color: Colors.white,
                        fontFamily: 'JosefinSans',
                        fontWeight: FontWeight.bold)),
                onPressed: () async {
                  if (_formKey.currentState.validate()) {
                    //setState(() => loading = true);
                    genre = selected;
                    nationalite = countSelected.name;
                    Categorie cat =
                        await categorieService.getCategory(selectedCat);
                    print('$cat 1');
                    dynamic result = await _auth
                        .signUp(widget.email, widget.password)
                        .then((currentUser) => _auth.createDocumment(
                            dateNaissance,
                            widget.email,
                            genre,
                            nationalite,
                            nom,
                            widget.password,
                            prenom,
                            tel,
                            photoUrl,
                            videoUrl,
                            description,
                            cat,
                            false));

                    if (result == null) {
                      setState(() {
                        //loading = false;
                        error = "this email is already used!";
                        //Navigator.push(context,MaterialPageRoute(builder: (context)=>ActualitePage()));
                      });
                    } else {
                      setState(() {
                        // loading = false;
                        Navigator.pushReplacementNamed(context, Home.id);
                      });

                      //ActualitePage();

                    }
                  }
                },
              ),
              SizedBox(
                height: 20,
              ),
              Text(error,
                  style: TextStyle(
                    color: Colors.red,
                  )),
            ],
          ),
        ),
      ),
    );
  }
}
