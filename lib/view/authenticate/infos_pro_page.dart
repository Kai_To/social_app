import 'package:flutter/material.dart';
import 'package:flutter_country_picker/country.dart';
import 'package:flutter_country_picker/flutter_country_picker.dart';
import 'package:social_app/repository/profAuth.dart';
import 'package:social_app/view/profil/pro_profile_page.dart';
import 'package:social_app/view/shared/loading.dart';
import 'package:social_app/view/shared/reusable_text_field.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:intl/intl.dart';

class InfosProPage extends StatefulWidget {
  final String email;
  final String password;
  InfosProPage({@required this.email, @required this.password});

  @override
  _InfosProPageState createState() => _InfosProPageState();
}

class _InfosProPageState extends State<InfosProPage> {
  final ProfAuth _profAuth = ProfAuth();
  final _formKey = GlobalKey<FormState>();
  String error = '';
  String nom = '';
  String prenom = '';
  String genre = '';
  String nationalite = '';
  String tel = '';
  String description = '';
  bool loading = false;
  String selected;
  Country countSelected;
  DateTime dateNaissance;
  final format = DateFormat("yyyy-MM-dd");

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return loading
        ? Loading()
        : Scaffold(
            body: Padding(
              padding: EdgeInsets.fromLTRB(20, 40, 20, 0),
              child: Container(
                  child: Form(
                      key: _formKey,
                      child: SingleChildScrollView(
                        child: Container(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: <Widget>[
                              Text(
                                'Profetionnel',
                                style: TextStyle(
                                    fontSize: 40,
                                    color: Colors.blueGrey,
                                    fontFamily: 'JosefinSans',
                                    fontWeight: FontWeight.bold),
                              ),
                              Container(
                                  child: CreateField(
                                fieldName: "Nom",
                                change: (val) {
                                  nom = val;
                                },
                              )),
                              SizedBox(
                                height: 15,
                              ),
                              Container(
                                  child: CreateField(
                                fieldName: "Prenom",
                                change: (val) {
                                  prenom = val;
                                },
                              )),
                              SizedBox(
                                height: 15,
                              ),
                              DropdownButtonFormField<String>(
                                  isDense: true,
                                  validator: (val) => val == null
                                      ? 'Choisissez votre genre'
                                      : null,
                                  style: TextStyle(
                                      fontSize: 17,
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold),
                                  decoration: InputDecoration(
                                    hintText: 'Genre',
                                    hintStyle: TextStyle(
                                        fontSize: 17,
                                        color: Colors.grey[600],
                                        fontWeight: FontWeight.bold),
                                  ),
                                  value: selected,
                                  items: ["homme", "femme"]
                                      .map((label) => DropdownMenuItem(
                                            child: Text(label),
                                            value: label,
                                          ))
                                      .toList(),
                                  onChanged: (value) {
                                    setState(() => selected = value);
                                  }),
                              SizedBox(
                                height: 15,
                              ),
                              CountryPicker(
                                currencyISOTextStyle: TextStyle(
                                    fontSize: 17,
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold),
                                currencyTextStyle: TextStyle(
                                    fontSize: 17,
                                    color: Colors.grey[600],
                                    fontWeight: FontWeight.bold),
                                nameTextStyle: TextStyle(
                                    fontSize: 17,
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold),
                                dense: false,
                                showFlag: true, //displays flag, true by default
                                showDialingCode:
                                    false, //displays dialing code, false by default
                                showName:
                                    true, //displays country name, true by default
                                showCurrency: false, //eg. 'British pound'
                                showCurrencyISO: true, //eg. 'GBP'
                                onChanged: (Country country) {
                                  setState(() {
                                    countSelected = country;
                                  });
                                },
                                selectedCountry: countSelected,
                              ),
                              SizedBox(
                                height: 15,
                              ),
                              Container(child: PhoneField(
                                onChange: (val) {
                                  tel = val;
                                },
                              )),
                              SizedBox(
                                height: 15,
                              ),
                              Container(
                                  child: CreateField(
                                fieldName: "Description",
                                change: (val) {
                                  description = val;
                                },
                              )),
                              SizedBox(
                                height: 15,
                              ),
                              DateTimeField(
                                  validator: (val) => val == null
                                      ? 'Choisissez votre date de naissance'
                                      : null,
                                decoration: InputDecoration(
                                  hintText: 'Date de Naissance',
                                  hintStyle: TextStyle(
                                      fontSize: 17,
                                      color: Colors.grey[600],
                                      fontWeight: FontWeight.bold),
                                ),
                                format: format,
                                onShowPicker: (context, currentValue) {
                                  return showDatePicker(
                                      context: context,
                                      firstDate: DateTime(1900),
                                      initialDate:
                                          currentValue ?? DateTime.now(),
                                      lastDate: DateTime(2100));
                                },
                                onChanged: (val) {
                                  dateNaissance = val;
                                  print(dateNaissance);
                                },
                              ),
                              SizedBox(
                                height: 30,
                              ),
                              RaisedButton(
                                  highlightColor: Colors.blue,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(30)),
                                  color: Colors.lightBlueAccent,
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 20, vertical: 10),
                                    child: Text('Register',
                                        style: TextStyle(
                                            fontSize: 25.0,
                                            color: Colors.white,
                                            fontFamily: 'JosefinSans',
                                            fontWeight: FontWeight.bold)),
                                  ),
                                  onPressed: () async {
                                    if (_formKey.currentState.validate()) {
                                      //setState(() => loading = true);
                                      genre = selected;
                                      nationalite = countSelected.name;
                                      dynamic result = await _profAuth
                                          .signUp(widget.email, widget.password)
                                          .then((currentUser) =>
                                              _profAuth.createDocument(
                                                  dateNaissance,
                                                  widget.email,
                                                  genre,
                                                  nationalite,
                                                  nom,
                                                  widget.password,
                                                  prenom,
                                                  tel,
                                                  description));

                                      if (result == null) {
                                        setState(() {
                                          //loading = false;
                                          error = "this email is already used!";
                                          //Navigator.push(context,MaterialPageRoute(builder: (context)=>ActualitePage()));
                                        });
                                      } else {
                                        setState(() {
                                          Navigator.pushReplacementNamed(
                                              context, ProProfilPage.id);
                                        });
                                      }
                                    }
                                  }),
                              SizedBox(
                                height: 20,
                              ),
                              Text(error,
                                  style: TextStyle(
                                    color: Colors.red,
                                  )),
                            ],
                          ),
                        ),
                      ))),
            ),
          );
  }
}
