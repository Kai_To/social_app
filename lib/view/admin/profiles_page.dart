
import 'package:flutter/material.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:social_app/model/Talent.dart';
import 'package:social_app/services/talentService.dart';
import 'package:social_app/view/shared/constants.dart';
import 'package:social_app/view/shared/profile_card.dart';
import 'package:social_app/view/shared/reusable_header.dart';



class ProfilesPage extends StatefulWidget {
  final bool validatedProfiles;

  const ProfilesPage({@required this.validatedProfiles});
  @override
  _ProfilesPageState createState() => _ProfilesPageState();
}

class _ProfilesPageState extends State<ProfilesPage> with AutomaticKeepAliveClientMixin<ProfilesPage>{
  TalentService talentService=TalentService();
  List<Talent> talents=[];
  bool isWaiting=false;
  List<Text> te=[];
  List<ProfileCard> profileCards=[];
  
  @override
  initState(){
    super.initState();
    isWaiting=true;
    getAllUsers();
  }

  getAllUsers()async{
    try {
      talents=await talentService.searchByIsValidated(widget.validatedProfiles);
    for(Talent t in talents){
  profileCards.add(ProfileCard(talent: t,));
    }
          
    setState(() {
      isWaiting=false;
    });
    } catch (e) {
      print(e);
       setState(() {
      isWaiting=false;
    });
    }
    
  }


  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      
      appBar: header(context, "admin", 'Profiles'),
        
      backgroundColor: Colors.grey[200],
      body:ModalProgressHUD(
              child: SingleChildScrollView(
                child: Column(
          
          children:profileCards,
        ),
              ), inAsyncCall: isWaiting,
      ) ,
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive =>true;
}



