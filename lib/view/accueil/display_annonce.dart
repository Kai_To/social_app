import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:social_app/model/Annonce.dart';
import 'package:social_app/model/Professionnel.dart';
import 'package:social_app/view/accueil/comments.dart';
import 'package:social_app/view/shared/constants.dart';
import 'package:social_app/services/annonceService.dart';
import 'package:social_app/services/commentaireService.dart';
import 'package:social_app/model/Commentaire.dart';
import 'package:social_app/services/notificationService.dart';
import 'package:social_app/model/Notification.dart';
import 'ajouter_annonce_page.dart';

class DisplayAnnonce extends StatefulWidget {
  final UserType userType;
  final Annonce annonce;
  final Professionnel prof;
  final bool isProf;
  DisplayAnnonce(
      {this.annonce,
      @required this.isProf,
      this.prof,
      @required this.userType});
  @override
  _DisplayAnnonceState createState() => _DisplayAnnonceState();
}

class _DisplayAnnonceState extends State<DisplayAnnonce> with AutomaticKeepAliveClientMixin<DisplayAnnonce> {
  AnnonceService annonceService = AnnonceService();
  CommentaireService commentaireService = CommentaireService();
  NotificationService notificationService = NotificationService();

  bool comment;
  String nom = '';
  String prenom = '';
  String descripton = '';
  String titre = '';
  String year = '';
  String day = '';
  String month = '';
  String hour = '';
  String min = '';
  String photoProfile = '';

  infos() {
    nom = widget.prof.nom;
    prenom = widget.prof.prenom;
    descripton = widget.annonce.description;
    titre = widget.annonce.titre;
    year = widget.annonce.date.year.toString();
    month = widget.annonce.date.month.toString();
    day = widget.annonce.date.day.toString();
    hour = widget.annonce.date.hour.toString();
    min = widget.annonce.date.minute.toString();
    photoProfile = widget.prof.photoProfile;
  }

  @override
  void initState() {
    super.initState();
    infos();
  }

  deletePost() async {
    int r = await annonceService.deleteAnnonce(widget.annonce.id);
    print(r + r);
    List<Commentaire> comments =
        await commentaireService.getComments(widget.annonce.id);
    for (Commentaire com in comments) {
      int c = await commentaireService.deleteCommentaire(com.uid);
      print(c);
    }
    List<Notifications> notifs =
        await notificationService.getNotifs(widget.annonce.id);
    for (Notifications not in notifs) {
      int c = await notificationService.deleteNotification(not.uid);
      print(c);
    }
    Navigator.of(context).pop();
  }

  deletePostAlert(context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: new Text("Etes-vous sure?"),
          actions: <Widget>[
            new FlatButton(
              child: new Text("supprimer"),
              onPressed: () => deletePost(),
            ),
            new FlatButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  modifierPost(context) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => AjouterAnnoncePage(
                  isUpd: true,
                  annonce: widget.annonce,
                )));
  }

  buildAnnonceContent(context) {
    return Container(
      
        child: Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Row(crossAxisAlignment: CrossAxisAlignment.start,
         children: <Widget>[
          Container(
              width: 40.0,
              height: 40.0,
              decoration: BoxDecoration(
                  color: Colors.grey,
                  image: DecorationImage(
                      image: photoProfile != ''
                          ? CachedNetworkImageProvider(photoProfile)
                          : AssetImage('assets/images/3.jpg'),
                      fit: BoxFit.cover),
                  borderRadius: BorderRadius.all(Radius.circular(20.0)),
                  boxShadow: [BoxShadow(blurRadius: 2.0, color: Colors.grey)])),
          SizedBox(
            width: 10,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                nom + ' ' + prenom,
                style: TextStyle(
                  fontSize: 15,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(
                width: 10,
              ),
              Text(
                day + '/' + month + '/' + year + '  ' + hour + ':' + min,
                style: TextStyle(fontSize: 10),
              )
            ],
          ),
          if (widget.isProf == true)
            PopupMenuButton<int>(
                padding: EdgeInsets.only(left: MediaQuery.of(context).size.width-185, bottom: 18),
                elevation: 3.2,
                itemBuilder: (context) => [
                  PopupMenuItem(
                    value: 1,
                    child: Text("Supprimer"),
                  ),
                  PopupMenuItem(
                    value: 2,
                    child: Text("Modifier"),
                  ),
                ],
                onSelected: (value) {
                  if (value == 1)
                    deletePostAlert(context);
                  else if (value == 2) modifierPost(context);
                },
                icon: Icon(Icons.more_horiz),
              )
        ]),
        Container(
          height: MediaQuery.of(context).size.height / 7,
          margin: EdgeInsets.symmetric(vertical: 5),
          padding: EdgeInsets.all(8),
          decoration: BoxDecoration(
              color: Colors.white,
              //border: Border.all(color: Colors.grey, width: 1),
              borderRadius: BorderRadius.circular(8)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                titre,
                style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  fontSize: 14,
                ),
              ),
              Text(
                descripton,
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 14,
                ),
              ),
            ],
          ),
        )
      ],
    ));
  }

  showComents({Annonce annonce, Professionnel prof}) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => Comments(
                  annonce: annonce,
                  prof: prof,
                  userType: widget.userType,
                )));
  }

  buildAnnonceFooter(context) {
    return Row(
      children: <Widget>[
        GestureDetector(
          onTap: () => showComents(annonce: widget.annonce, prof: widget.prof),
          child: Icon(
            Icons.chat,
            //color: Colors.blue,
            size: 20,
          ),
        ),
        SizedBox(
          width: 4,
        ),
        Container(
          child: Text(
            'commentaires',
            style: TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.bold,
            ),
          ),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 0, vertical: 1.5),
      decoration: BoxDecoration(
        color: Colors.white,
        // border: Border.all(color: Colors.grey[400], width: 1),
      ),
      padding: EdgeInsets.all(10),
      child: Column(
        children: <Widget>[
          buildAnnonceContent(context),
          buildAnnonceFooter(context),
        ],
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
