import 'package:flutter/material.dart';
import 'package:social_app/model/Annonce.dart';
import 'package:social_app/model/Professionnel.dart';
import 'package:social_app/services/annonceService.dart';
import 'package:social_app/services/professionelService.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:social_app/view/shared/constants.dart';
import 'package:social_app/view/shared/reusable_header.dart';

class AjouterAnnoncePage extends StatefulWidget {
  final bool isUpd;
  final Annonce annonce;

  const AjouterAnnoncePage({this.annonce, this.isUpd});
  @override
  _AjouterAnnoncePageState createState() => _AjouterAnnoncePageState();
}

class _AjouterAnnoncePageState extends State<AjouterAnnoncePage> {
  String titre = '';
  String contenu = '';
  AnnonceService annonceService = AnnonceService();
  TextEditingController controller = TextEditingController();
  TextEditingController controller2 = TextEditingController();

  GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  bool isWaiting = false;
  @override
  void initState() {
    if (widget.isUpd == true)
      controller = TextEditingController(text: widget.annonce.description);
    if (widget.isUpd == true)
      controller2 = TextEditingController(text: widget.annonce.titre);
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: widget.isUpd == true
          ? AppBar(
              title: Text(
                "Modifier Annonce",
                style: kAdminHeaderTextStyle,
              ),
            )
          : header(context, "Ajouter Annonce", 'Ajouter Annonce'),
      backgroundColor: Colors.grey[300],
      body: ModalProgressHUD(
        child: SingleChildScrollView(
          child: Container(
            height: MediaQuery.of(context).size.height,
            color: Colors.white,
            margin: EdgeInsets.symmetric(horizontal: 10),
            padding: EdgeInsets.all(20),
            child: Form(
              key: _formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.all(15),
                    child: TextFormField(
                      controller: controller2,
                      validator: (String value) {
                        if (value.length == 0) {
                          return "Entrez un titre";
                        }
                        return null;
                      },
                      cursorColor: Colors.purple,
                      decoration: InputDecoration(
                          focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.purple)),
                          focusColor: Colors.black,
                          fillColor: Colors.black,
                          hoverColor: Colors.black,
                          hintText: 'titre',
                          hintStyle: TextStyle(color: Colors.black38),
                          icon: Icon(
                            Icons.title,
                            color: Colors.grey[700],
                            size: 20,
                          ),
                          border: UnderlineInputBorder()),
                    ),
                  ),
                  Container(
                      child: TextFormField(
                    controller: controller,
                    cursorColor: Colors.purple,
                    validator: (String value) {
                      if (value.length < 30) {
                        return "Entrez plus que 30 caracteres ";
                      }
                      return null;
                    },
                    decoration: InputDecoration(
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.purple)),
                        hintText: 'Entrez votre annonce ici',
                        border: OutlineInputBorder()),
                    maxLines: 10,
                  )),
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                    height: 50,
                    width: 120,
                    child: RaisedButton(
                      child: widget.isUpd == true
                          ? Text(
                              'Enregistrer',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontFamily: 'JosefinSans',
                                color: Colors.white,
                                fontSize: 20,
                              ),
                            )
                          : Text(
                              'Publier',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontFamily: 'JosefinSans',
                                color: Colors.white,
                                fontSize: 20,
                              ),
                            ),
                      color: widget.isUpd == true
                          ? Colors.lightBlueAccent
                          : Colors.purple[900],
                      onPressed: () async {
                        if (_formKey.currentState.validate()) {
                          if (_formKey.currentState.validate()) {
                            _formKey.currentState.save();
                            setState(() {
                              isWaiting = true;
                            });
                            if (widget.isUpd == true) {
                              await updateAnnonce();
                              setState(() {
                                Navigator.pop(context as Element);
                              });
                            } else {
                              await addAnnonce();
                            }
                            setState(() {
                              this.controller.clear();
                              isWaiting = false;
                            });
                          }
                        }
                      },
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
        inAsyncCall: isWaiting,
      ),
    );
  }

  addAnnonce() async {
    Professionnel pro = await ProfessionelService().getCurrentPro();
    Annonce annonce = Annonce(
        proRef: pro.proID,
        description: controller.text,
        date: DateTime.now(),
        titre: titre);
    int a = 0;
    a = await annonceService.createAnnonce(annonce);
    setState(() {
      isWaiting = false;
    });
    if (a == 0) print('erreur:annonce non publiée!!');
  }

  updateAnnonce() async {
    int r = await annonceService.updateAnnonce(Annonce(
      titre: controller2.text,
      description: controller.text,
      date: widget.annonce.date,
      id: widget.annonce.id,
      proRef: widget.annonce.proRef,
    ));
    print(r);
    setState(() {
      isWaiting = false;
    });
  }
}
