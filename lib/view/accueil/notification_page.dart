import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:social_app/model/Commentaire.dart';
import 'package:social_app/model/Professionnel.dart';
import 'package:social_app/repository/NotificationRepositoryImpl.dart';
import 'package:social_app/services/notificationService.dart';
import 'package:social_app/services/professionelService.dart';
import 'package:social_app/view/accueil/comments.dart';
import 'package:social_app/view/shared/constants.dart';
import 'package:social_app/view/shared/reusable_header.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'package:social_app/view/profil/profil_page.dart';

class NotificationPage extends StatefulWidget {
  @override
  _NotificationPageState createState() => _NotificationPageState();
}

class _NotificationPageState extends State<NotificationPage> {
  NotificationService notificationService = NotificationService();
  ProfessionelService proService = ProfessionelService();
  Professionnel pro;
  bool isWaiting = false;

  @override
  void initState() {
    super.initState();
    isWaiting = true;
    getCurrentPro();
  }

  getCurrentPro() async {
    try {
      pro = await proService.getCurrentPro();
      setState(() {
        isWaiting = false;
      });
    } catch (e) {
      print(e);
      setState(() {
        isWaiting = false;
      });
    }

    buildNotif();
  }

  getNotifs() async {
    var snapshot = await notifRef
        .where("professionnel.uid", isEqualTo: pro.proID)
        .limit(40)
        //.orderBy('date', descending: true)
        .getDocuments();
    List<DisplayNotifs> notifs = [];
    snapshot.documents.forEach((doc) {
      notifs.add(DisplayNotifs.fromDocument(doc));
    });
    return notifs;
  }

  buildNotif() {
    return FutureBuilder(
        future: getNotifs(),
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return Text('');
          }
          return ListView(
            children: snapshot.data,
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: header(context, 'Notification', 'Notifications'),
      body: ModalProgressHUD(
        child: Column(
          children: <Widget>[
            Expanded(
              child: buildNotif(),
            ),
            Divider(),
          ],
        ),
        inAsyncCall: isWaiting,
      ),
    );
  }
}

class DisplayNotifs extends StatelessWidget {
  final Timestamp date;
  final Commentaire commentaire;
  //final bool wasSeen;

  DisplayNotifs({this.commentaire, this.date});

  factory DisplayNotifs.fromDocument(DocumentSnapshot doc) {
    return DisplayNotifs(
      commentaire: Commentaire.fromMap(doc['commentaire']),
      date: doc['date'],
      //wasSeen: doc['wasSeen'],
    );
  }

  goToProfile(context) {
  Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => ProfilPage(
                poster: commentaire.talent,
                visitor: UserType.professionnel, 
                isProfil: false,
              )));
}

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(bottom: 2),
      child: GestureDetector(
        onTap: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => Comments(
                        annonce: commentaire.annonce,
                        userType: UserType.professionnel,
                      )));
        },
        child: Container(
          color: Colors.white54,
          child: ListTile(
            title: GestureDetector(
              child: RichText(
                overflow: TextOverflow.ellipsis,
                text: TextSpan(
                    style: TextStyle(fontSize: 14, color: Colors.black),
                    children: [
                      TextSpan(
                          text: commentaire.talent.nom,
                          style: TextStyle(fontWeight: FontWeight.bold)),
                      TextSpan(text: ' a commenté votre annonce'),
                    ]),
              ),
            ),
            leading: GestureDetector(
              onTap: ()=> goToProfile(context),
              child: CircleAvatar(
                backgroundImage: commentaire.talent.photoProfile != null
                    ? CachedNetworkImageProvider(
                        commentaire.talent.photoProfile)
                    : AssetImage('assets/images/3.jpg'),
              ),
            ),
            subtitle: Text(
              timeago.format(date.toDate()),
              style: TextStyle(
                color: Colors.black38,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
