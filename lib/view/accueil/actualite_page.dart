import 'package:flutter/material.dart';
import 'package:social_app/model/Post.dart';
import 'package:social_app/model/Talent.dart';
import 'package:social_app/services/postService.dart';
import 'package:social_app/services/talentService.dart';
import 'package:social_app/view/profil/post_widget.dart';
import 'package:social_app/view/shared/progress.dart';
import 'package:social_app/view/shared/reusable_header.dart';
import 'package:social_app/view/shared/constants.dart';
class ActualitePage extends StatefulWidget {
  static const String id = 'actualite_page';
  final UserType userType;

 ActualitePage({this.userType});
  
  @override
  _ActualitePageState createState() => _ActualitePageState();
}

class _ActualitePageState extends State<ActualitePage> with AutomaticKeepAliveClientMixin<ActualitePage>  {
  bool isWaiting = false;
  Talent talent = Talent();
  List<Post> usersPosts = [];
  List<PostWidget> postWidgets = [];
  PostService postService = PostService();

  @override
  void initState() {
    super.initState();
    if (postWidgets.isEmpty) isWaiting = true;
    getUserAndPosts();
  }
  @override
  void dispose() {
    usersPosts=[];
    postWidgets=[];
    isWaiting=false;
    super.dispose();
  }

  getUserAndPosts() async {
    usersPosts = await postService.getallPosts();
    await generateList();
    setState(() {
      isWaiting = false;
    });
  }

  generateList() async {
    for (Post post in usersPosts) {
      Talent poster = await TalentService().searchById(post.talentId);
      if(poster.isValidated == true){
      setState(() {
        postWidgets.add(PostWidget(
          post: post,
          poster: poster, 
          canDelete: false,
          userType: widget.userType,
        ));
      });
      }
    }
  }

  Widget alternativeScreen() {
    if (isWaiting)
      return circularProgress();
    else
      return Center(
        child: Text("pas de resultats"),
      );
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
        appBar: header(context, "actualite", 'Artness'),
        backgroundColor: Colors.grey[300],
        body: isWaiting || postWidgets.isEmpty
            ? alternativeScreen()
            : RefreshIndicator(
                          child: SingleChildScrollView(
                  child: Column(
                  children: postWidgets,
                )), onRefresh: ()async {
                  isWaiting = true;
  
                   refresh();},
            ));
  }

   Future<void> refresh() async {
    setState(() {
      isWaiting=true;
    });
    this.postWidgets=[];
    await getUserAndPosts();
    
  
    
  }

  @override
  bool get wantKeepAlive => true;
}
