import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:social_app/model/Annonce.dart';
import 'package:social_app/model/Commentaire.dart';
import 'package:social_app/model/Notification.dart';
import 'package:social_app/model/Professionnel.dart';
import 'package:social_app/model/Talent.dart';
import 'package:social_app/repository/commentaireRepositoryImpl.dart';
import 'package:social_app/services/commentaireService.dart';
import 'package:social_app/services/notificationService.dart';
import 'package:social_app/services/professionelService.dart';
import 'package:social_app/services/talentService.dart';
import 'package:social_app/view/shared/constants.dart';
import 'package:social_app/view/shared/reusable_header.dart';
import 'package:timeago/timeago.dart' as timeago;

class Comments extends StatefulWidget {
  final UserType userType;
  final Annonce annonce;
  final Professionnel prof;

  Comments({this.prof, this.annonce, @required this.userType});
  @override
  _CommentsState createState() => _CommentsState();
}

class _CommentsState extends State<Comments> {
  CommentaireService commentaireService = CommentaireService();
  TextEditingController commentController = TextEditingController();
  ProfessionelService proService = ProfessionelService();
  NotificationService notificationService = NotificationService();
  bool isWaiting = false;

  @override
  void initState() {
    super.initState();
  }

  //**Display comments */
  buildComment() {
    return StreamBuilder(
        stream: commentRef
            .where('annonce.uid', isEqualTo: widget.annonce.id)
            //.orderBy('document.date', descending: false)
            .snapshots(),
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return Center(child: Text('no comments'));
          }
          List<Comment> comments = [];
          snapshot.data.documents.forEach((doc) {
            comments.add(Comment.fromDocument(doc));
          });
          return ListView(
            children: comments,
          );
        });
  }

  //***add comments */
  addComment() async {
    Talent talent = await TalentService().getCurrentUser();
    Commentaire commentaire = Commentaire(
      annonce: widget.annonce,
      contenue: commentController.text,
      date: DateTime.now(),
      talent: talent,
    );
    int r = 0;
    r = await commentaireService.createCommentaire(commentaire);
    await addCommentToNotification(talent, commentaire);
    if (r == 1) commentController.clear();
  }

  //***Notification */
  addCommentToNotification(talent, commentaire) async {
    Notifications notif = Notifications(
        date: DateTime.now(),
        commentaire: commentaire,
        professionnel: widget.prof);
    int r = await notificationService.createNotification(notif);
    print(r);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: header(context, 'Comments', 'Commentaires'),
      body: Column(
        //crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Expanded(
            child: buildComment(),
          ),
          Divider(),
          if (widget.userType == UserType.talent)
            ListTile(
              title: TextFormField(
                controller: commentController,
                decoration: InputDecoration(
                  hintText: 'écris un commentaire ...',
                ),
              ),
              trailing: FlatButton.icon(
                  onPressed: addComment,
                  icon: Icon(
                    Icons.send,
                    color: Colors.blueAccent,
                    size: 30,
                  ),
                  label: Text('')),
            )
        ],
      ),
    );
  }
}

//**********Comment widget */

class Comment extends StatelessWidget {
  final Talent talent;
  final String contenue;
  final Timestamp date;

  Comment({this.contenue, this.talent, this.date});

  factory Comment.fromDocument(DocumentSnapshot doc) {
    return Comment(
      talent: Talent.fromMap(doc['talent']),
      contenue: doc['contenue'],
      date: doc['date'],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        ListTile(
          title: Row(
            children: <Widget>[
              Text(
                talent.nom,
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              ),
              SizedBox(
                width: 10,
              ),
              Text(
                timeago.format(date.toDate()),
                style: TextStyle(
                  color: Colors.black38,
                ),
              ),
            ],
          ),
          leading: CircleAvatar(
            backgroundImage: talent.photoProfile != null
                ? CachedNetworkImageProvider(talent.photoProfile)
                : AssetImage('assets/images/3.jpg'),
          ),
          subtitle: Text(
            contenue,
            style: TextStyle(fontSize: 18),
          ),
        ),
        Divider(),
      ],
    );
  }
}
